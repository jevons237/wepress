# wepress

## 介绍

一个基于 JPress 的微信小程序。JPress 是一个类似 WordPress 的建站神器，支持文章、页面、商品、会员等管理。

## 软件架构
- UI ：https://github.com/youzan/vant-weapp


## 安装教程

1.  git clone https://gitee.com/fuhai/wepress.git
2.  导入微信开发者工具
3.  使用 npm 构建


## 注意事项

**开启 JPress API 功能**

进入 JPress 后台，在 `系统 -> 接口` 里开启 API 接口，并填写应用ID 和 签名密钥。



**配置 WePress 的 config.js**

在 wepress 的根目录下，找到 config.js ，填写 JPress 可以访问的域名、app_id 和 app_secret。务必要保证 config.js 里的 app_id 和 JPress 后台
填写的 应用ID 一致。 app_secret 和 签名密钥 一致。



**微信开发工具配置**

- 在项目详情里，开启 NPM 模块
- 在项目详情里，开启不校验域名合法性