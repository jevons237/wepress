var config = require('./config.js');
var jpress = require('./utils/jpress.js');
App({
  onLaunch: function () {
    jpress.init(config.config);
  },
  globalData: {
    userInfo: null
  }
})